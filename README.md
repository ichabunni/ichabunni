# Hello 👋 I'm Melissa! Nice to meet you.

I'm a [designer](https://melissahie.com/), [casual travel writer](https://girleatworld.net), occasional coder, [food enthusiast](https://instagram.com/girleatworld) and last but not least, a mother.
